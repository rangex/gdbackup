#!/usr/bin/php
<?php
require_once __DIR__ . '/../src/GDBackUp.php';
$configuration = json_decode(file_get_contents(__DIR__ . '/../config/config.json'), true);

if ($_SERVER['argc'] != 2 || in_array('-h', $_SERVER['argv']) || in_array('--help', $_SERVER['argv'])) {
	echo "Usage: gdrive-fileinfo.php file_id\n";
	echo "file_id: Google ID of the file to get info on.\n";
	exit(1);
} else {
	try {
		$gd = new GDBackUp($configuration['gd_client_id'], $configuration['gd_service_account_name'], $configuration['gd_key_path'], $configuration['gd_delegated_account']);
		$files = $gd->get($_SERVER['argv'][1]);
	
		$perms = $gd->list_permissions($_SERVER['argv'][1]);

		echo "\n*** Metadata ***\n";
		printf("File Name: %s\n", $files['title']);
		printf("File ID: %s\n", $files['id']);
		printf("File MD-5: %s\n", $files['md5Checksum']);
		printf("File MIME-Type: %s\n", $files['mimeType']);
		if ($files['mimeType'] != "application/vnd.google-apps.folder") {
			printf("File Size: %s bytes\n\n", $files['fileSize']);
			echo "*** Links ***\n";
			printf("Direct File URL (Requires oAuth token): %s\n\n", $files['downloadUrl']);
			printf("Web Download URL (Requires Google login unless file is public): %s\n\n", $files['webContentLink']);
			printf("Viewer/Embed URL: %s\n\n", $files['alternateLink']);

		}	
		echo "*** Permissions ***\n";
		foreach ($perms['items'] as $perm) {
			printf("%s (%s) - %s\n", $perm['name'], $perm['emailAddress'], $perm['role']);
		}

	} catch (Exception $e) {
		printf("%s\n", $e->getMessage());
	}
}
?>