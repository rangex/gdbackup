#!/usr/bin/php
<?php

if ($_SERVER['argc'] > 2 || in_array('-h', $_SERVER['argv']) || in_array('--help', $_SERVER['argv'])) {
	echo "Usage: gdrive-listfiles.php folder_id\n";
	echo "folder_id: path to the folder to list the files under. If omitted, will get the root Google Drive folder.\n";
	exit(1);
}

require_once __DIR__ . '/../src/GDBackUp.php';
$configuration = json_decode(file_get_contents(__DIR__ . '/../config/config.json'), true);

	$parameters = array();
	$parameters['maxResults'] = '1000';
	if( $_SERVER['argc'] != 2 ) {
		$parameters['q'] = "'root' in parents";
	} else {
		$parameters['q'] = "'".$_SERVER['argv'][1]."' in parents";
	}

// Then, make the Google request.
	try {
		$gd = new GDBackUp($configuration['gd_client_id'], $configuration['gd_service_account_name'], $configuration['gd_key_path'], $configuration['gd_delegated_account']);
		$files = $gd->listFiles($parameters);

	// Then, sort the results alphabetically into array $sortedfiles.

		$sortedfiles = array();
		foreach ($files['items'] as $f) {
			array_push($sortedfiles, $f);
		}

		usort($sortedfiles, function($a, $b) {
			return strcasecmp($a['title'], $b['title']);
		});
	
	// Finally, parse the array and produce a nice table.
	
		foreach ($sortedfiles as $item) {
			$starred = "-";
			$trashed = "-";
			$restricted = "-";
			$viewed = "-";
			if ($item['labels']['starred'] == "1") {
				$starred = "s";
			}
			
			if ($item['labels']['trashed'] == "1") {
				$trashed = "t";
			}
			if ($item['labels']['restricted'] == "1") {
				$restricted = "r";
			}
			if ($item['labels']['viewed'] == "1") {
				$viewed = "v";
			}
			
			
			if ($item['mimeType'] == "application/vnd.google-apps.folder") {
				printf("%s%s%s%s - %s - FOLDER - %s\n", $starred, $trashed, $restricted, $viewed, $item['id'], $item['title']);
			} else {
				printf("%s%s%s%s - %s - %s bytes - %s\n", $starred, $trashed, $restricted, $viewed, $item['id'], $item['fileSize'], $item['title']);			
				
			}
		}
	} catch (Exception $e) {
		printf("%s\n", $e->getMessage());
		exit(1);
	}
?>