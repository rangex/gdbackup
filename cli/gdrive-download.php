#!/usr/bin/php
<?php
ob_implicit_flush(1);
set_time_limit(0);
require_once __DIR__ . '/../src/GDBackUp.php';
$configuration = json_decode(file_get_contents(__DIR__ . '/../config/config.json'), true);

if ($_SERVER['argc'] != 3 || in_array('-h', $_SERVER['argv']) || in_array('--help', $_SERVER['argv'])) {
    echo "Usage: gdrive-download.php file_id save_path\n";
    echo "file_id: Google ID of the file to get info on.\n";
    echo "save_path: Folder to save the file to. Please do not use a shell relative path, e.g. ~\n";
    exit(1);
} else {
    try {
        echo "Downloading file...\n";
        $gd = new GDBackUp($configuration['gd_client_id'], $configuration['gd_service_account_name'], $configuration['gd_key_path'], $configuration['gd_delegated_account']);
        $dl = $gd->download($_SERVER['argv'][1], $_SERVER['argv'][2]);

        printf("%s bytes written.\n", (string)$dl);


    } catch (Exception $e) {
        printf("%s\n", $e->getMessage());
        exit(1);
    }
}
?>